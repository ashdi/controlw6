﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Http;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class OrderController : ApiController
    {
        OrderDbContext db = new OrderDbContext();

        public IEnumerable<Order> GetOrder()
        {
            return db.Order;
        }

        public Order GetOrder(int id)
        {
            Order order = db.Order.Find(id);
            return order;
        }

        [HttpPost]
        public void CreateOrder([FromBody]Order order)
        {
            db.Order.Add(order);
            db.SaveChanges();
        }

        [HttpPut]
        public void EditOrder(int id, [FromBody]Order order)
        {
            if (id == order.Id)
            {
                db.Entry(order).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteOrder(int id)
        {
            Order order = db.Order.Find(id);
            if (order != null)
            {
                db.Order.Remove(order);
                db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}