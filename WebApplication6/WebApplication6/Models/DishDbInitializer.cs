﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class DishDbInitializer : DropCreateDatabaseAlways<DishDbContext>

    {
        protected override void Seed(DishDbContext context)
        {
            context.Dish.Add(new Dish { Id = 1, Description = "Жесткие диски", Name = "HDD", Price = 110m });
            context.Dish.Add(new Dish { Id = 2, Description = "Мониторы", Name = "Monitors", Price = 120m });
            context.Dish.Add(new Dish { Id = 3, Description = "Материнские платы", Name = "Motherboards", Price = 130m });
            context.Dish.Add(new Dish { Id = 4, Description = "Оперативная память", Name = "DDR", Price = 910m });

            
            base.Seed(context);
        }
    }
}