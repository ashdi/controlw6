﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class ClientController : Controller
    {
        ClientDbContext db = new ClientDbContext();

        public IEnumerable<Client> GetClient()
        {
            return db.Client;
        }

        public Client GetClient(int id)
        {
            Client Client = db.Client.Find(id);
            return Client;
        }

        [System.Web.Http.HttpPost]
        public void CreateClient([FromBody]Client Client)
        {
            db.Client.Add(Client);
            db.SaveChanges();
        }

        [System.Web.Http.HttpPut]
        public void EditClient(int id, [FromBody]Client Client)
        {
            if (id == Client.Id)
            {
                db.Entry(Client).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteClient(int id)
        {
            Client Client = db.Client.Find(id);
            if (Client != null)
            {
                db.Client.Remove(Client);
                db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}