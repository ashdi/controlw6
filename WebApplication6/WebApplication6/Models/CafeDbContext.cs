﻿using Newtonsoft.Json;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace WebApplication6.Models
{
    public class CafeDbContext : DbContext
    {
        public DbSet<Cafe> Cafe { get; set; }
    }
}
    