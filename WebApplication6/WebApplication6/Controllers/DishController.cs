﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class DishController : ApiController
    {
        DishDbContext db = new DishDbContext();

        public IEnumerable<Dish> GetDish()
        {
            return db.Dish;
        }

        public Dish GetDish(int id)
        {
            Dish Dish = db.Dish.Find(id);
            return Dish;
        }

        [System.Web.Http.HttpPost]
        public void CreateDish([FromBody]Dish Dish)
        {
            db.Dish.Add(Dish);
            db.SaveChanges();
        }

        [System.Web.Http.HttpPut]
        public void EditDish(int id, [FromBody]Dish Dish)
        {
            if (id == Dish.Id)
            {
                db.Entry(Dish).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteDish(int id)
        {
            Dish Dish = db.Dish.Find(id);
            if (Dish != null)
            {
                db.Dish.Remove(Dish);
                db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}