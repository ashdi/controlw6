﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class CafeDbInitializer : DropCreateDatabaseAlways<CafeDbContext>
    {
        protected override void Seed(CafeDbContext context)
        {
            context.Cafe.Add(new Cafe { Id = 1, Name = "Samsung", Description = "fun" });
            context.Cafe.Add(new Cafe { Id = 2, Name = "ASUS", Description = "cool" });
            context.Cafe.Add(new Cafe { Id = 3, Name = "Acer", Description = "cold" });
            context.Cafe.Add(new Cafe { Id = 4, Name = "HP", Description = "gold" });
            base.Seed(context);
        }
    }
}