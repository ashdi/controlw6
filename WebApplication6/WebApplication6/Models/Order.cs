﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication6.Models
{
    public class Order : Entity
    {

        [ForeignKey("Client")]
        [FromForm(Name = "ClientId")]
        public int clientId { get; set; }
        public Client Client { get; set; }

        [ForeignKey("Dish")]
        [FromForm(Name = "DishId")]
        public int dishId { get; set; }
        public Dish Dish { get; set; }

        public DateTime ActionDate { get; set; }
    }
}