﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class CafeInitializer : DropCreateDatabaseAlways<CafeDbContext>
    {
        

        /*

        protected override void Seed(CafeDbContext context)
        {
            context.Cafe.Add(new Cafe{ Id = 1, Name = "Samsung", Description = "fun" });
            context.Cafe.Add(new Cafe { Id = 2, Name = "ASUS", Description = "cool" });
            context.Cafe.Add(new Cafe { Id = 3, Name = "Acer", Description = "cold" } );
            context.Cafe.Add(new Cafe { Id = 4, Name = "HP", Description = "gold" } );
            context.Client.AddRange(new Client{ Id = 1, Name = "S1", Contacts = "ушастый" },
                        new Client { Id = 2, Name = "S1", Contacts = "лысый" },
                        new Client { Id = 3, Name = "S1", Contacts = "волосатый" },
                        new Client { Id = 4, Name = "S1", Contacts = "лупоглазый" });
            context.Order.AddRange(new Order{ Id = 1, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 2 },
                       new Order {Id = 2, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 3, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 1, clientId = 3 },
                       new Order {Id = 4, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 5, ActionDate = new DateTime(2015, 7, 30, 9, 30, 0, 0, DateTimeKind.Unspecified), dishId = 2, clientId = 1 },
                       new Order {Id = 6, ActionDate = new DateTime(2015, 7, 30, 9, 30, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 2 },
                       new Order {Id = 7, ActionDate = new DateTime(2015, 7, 30, 9, 30, 0, 0, DateTimeKind.Unspecified), dishId = 4, clientId = 3 },
                       new Order {Id = 8, ActionDate = new DateTime(2015, 8, 14, 15, 30, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 9, ActionDate = new DateTime(2015, 8, 14, 15, 30, 0, 0, DateTimeKind.Unspecified), dishId = 1, clientId = 2 },
                       new Order {Id = 10, ActionDate = new DateTime(2015, 8, 20, 10, 5, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 3 },
                       new Order {Id = 11, ActionDate = new DateTime(2015, 8, 25, 12, 15, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 12, ActionDate = new DateTime(2015, 8, 25, 12, 15, 0, 0, DateTimeKind.Unspecified), dishId = 2, clientId = 1 },
                       new Order {Id = 13, ActionDate = new DateTime(2015, 8, 25, 12, 15, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 2 },
                       new Order {Id = 14, ActionDate = new DateTime(2015, 8, 25, 14, 33, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 15, ActionDate = new DateTime(2015, 10, 4, 12, 50, 0, 0, DateTimeKind.Unspecified), dishId = 2, clientId = 4 },
                       new Order {Id = 16, ActionDate = new DateTime(2015, 10, 27, 9, 18, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 17, ActionDate = new DateTime(2015, 12, 11, 9, 20, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 18, ActionDate = new DateTime(2016, 2, 7, 9, 43, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 19, ActionDate = new DateTime(2016, 3, 12, 9, 21, 0, 0, DateTimeKind.Unspecified), dishId = 2, clientId = 1 },
                       new Order {Id = 20, ActionDate = new DateTime(2016, 6, 28, 8, 55, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 4 },
                       new Order {Id = 21, ActionDate = new DateTime(2016, 7, 15, 10, 2, 0, 0, DateTimeKind.Unspecified), dishId = 2, clientId = 1 },
                       new Order {Id = 22, ActionDate = new DateTime(2016, 7, 16, 9, 57, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 23, ActionDate = new DateTime(2016, 9, 15, 9, 15, 0, 0, DateTimeKind.Unspecified), dishId = 4, clientId = 2 },
                       new Order {Id = 24, ActionDate = new DateTime(2016, 9, 20, 10, 3, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 25, ActionDate = new DateTime(2016, 10, 2, 11, 10, 0, 0, DateTimeKind.Unspecified), dishId = 1, clientId = 3 },
                       new Order {Id = 26, ActionDate = new DateTime(2016, 10, 23, 10, 2, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 },
                       new Order {Id = 27, ActionDate = new DateTime(2017, 3, 10, 13, 23, 0, 0, DateTimeKind.Unspecified), dishId = 4, clientId = 4 });
            context.Dish.Add(new Dish { Id = 1, Description = "Жесткие диски", Name = "HDD", Price = 110m });
            context.Dish.Add(new Dish { Id = 2, Description = "Мониторы", Name = "Monitors", Price = 120m });
            context.Dish.Add(new Dish { Id = 3, Description = "Материнские платы", Name = "Motherboards", Price = 130m });
            context.Dish.Add(new Dish { Id = 4, Description = "Оперативная память", Name = "DDR", Price = 910m });

            base.Seed(context);
        }*/
        protected override void Seed(CafeDbContext context)
        {
            context.Cafe.Add(new Cafe { Id = 1, Name = "Samsung", Description = "fun" });
            context.Cafe.Add(new Cafe { Id = 2, Name = "ASUS", Description = "cool" });
            context.Cafe.Add(new Cafe { Id = 3, Name = "Acer", Description = "cold" });
            context.Cafe.Add(new Cafe { Id = 4, Name = "HP", Description = "gold" });
            base.Seed(context);
        }
    }
}

/*
 
     public class BookDbInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext db)
        {
            db.Books.Add(new Book { Name = "Война и мир", Author = "Л. Толстой", Year = 1863 });
            db.Books.Add(new Book { Name = "Отцы и дети", Author = "И. Тургенев", Year = 1862 });
            db.Books.Add(new Book { Name = "Чайка", Author = "А. Чехов", Year = 1896 });
 
            base.Seed(db);
        }*/
