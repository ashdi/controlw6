﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class CafeController : Controller
    {
        CafeDbContext db = new CafeDbContext();

        public IEnumerable<Cafe> GetCafe()
        {
            return db.Cafe;
        }

        public Cafe GetCafe(int id)
        {
            Cafe Cafe = db.Cafe.Find(id);
            return Cafe;
        }

        [System.Web.Http.HttpPost]
        public void CreateCafe([FromBody]Cafe Cafe)
        {
            db.Cafe.Add(Cafe);
            db.SaveChanges();
        }

        [System.Web.Http.HttpPut]
        public void EditCafe(int id, [FromBody]Cafe Cafe)
        {
            if (id == Cafe.Id)
            {
                db.Entry(Cafe).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteCafe(int id)
        {
            Cafe Cafe = db.Cafe.Find(id);
            if (Cafe != null)
            {
                db.Cafe.Remove(Cafe);
                db.SaveChanges();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}