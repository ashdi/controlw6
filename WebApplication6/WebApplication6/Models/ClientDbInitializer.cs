﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class ClientDbInitializer : DropCreateDatabaseAlways<ClientDbContext>
    {
        protected override void Seed(ClientDbContext context)
        {
            context.Client.Add(new Client { Id = 1, Name = "S1", Contacts = "ушастый" });
            context.Client.Add(new Client { Id = 2, Name = "S11", Contacts = "лысый" });
            context.Client.Add(new Client { Id = 3, Name = "S111", Contacts = "волосатый" });
            context.Client.Add(new Client { Id = 4, Name = "S1111", Contacts = "лупоглазый" });
           
            base.Seed(context);
        }
    }
}