﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class OrderDbInitializer : DropCreateDatabaseAlways<OrderDbContext>
    {
        protected override void Seed(OrderDbContext context)
        {
            NewMethod(context);

            base.Seed(context);
        }

        private static void NewMethod(OrderDbContext context)
        {
            context.Order.Add(new Order { Id = 1, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 2 });
            context.Order.Add(new Order { Id = 3, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 1, clientId = 3 });
            context.Order.Add(new Order { Id = 5, ActionDate = new DateTime(2015, 7, 30, 9, 30, 0, 0, DateTimeKind.Unspecified), dishId = 2, clientId = 1 });
            context.Order.Add(new Order { Id = 4, ActionDate = new DateTime(2015, 7, 25, 12, 30, 33, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 1 });
            context.Order.Add(new Order { Id = 6, ActionDate = new DateTime(2015, 7, 30, 9, 30, 0, 0, DateTimeKind.Unspecified), dishId = 3, clientId = 2 });
        }
    }
}